# Maven – JaCoCo code coverage example
Maven, JUnit 5 + JaCoCo example.

Project Link - https://www.mkyong.com/maven/maven-jacoco-code-coverage-example/

## How to run this project?
```

$ git clone https://github.com/mkyong/maven-examples.git
$ cd maven-code-coverage
$ mvn clean test

# view report at 'target/site/jacoco/index.html'
hello
```


[![coverage report](https://gitlab.com/rovinvadivel/java-maven-junit-coverage/badges/master/coverage.svg)](https://gitlab.com/rovinvadivel/java-maven-junit-coverage/-/commits/master)


[![pipeline status](https://gitlab.com/rovinvadivel/java-maven-junit-coverage/badges/master/pipeline.svg)](https://gitlab.com/rovinvadivel/java-maven-junit-coverage/-/commits/master)
